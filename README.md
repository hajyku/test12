### Rake task
```bash
bundle exec rake cbr
```

### Bearer auth
Вначале создать пользователя (password не менее 7 символов)
```
rails c
> User.create(email:'test@test.com', password:'test111')
```

выполнить rake-задачу на получение данных
```
bundle exec rake cbr
```

Получить токен
```
curl -X POST http://localhost:3000/api/v1/login \
-H 'content-type: multipart/form-data' \
-F 'email=test@test.com' -F 'password=test111'
```

Список валют
```bash
curl http://127.0.0.1:3000/api/v1/currencies \
-H 'content-type: multipart/form-data' \
-H 'Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxLCJleHBpcnkiOjE1ODUwNzU5ODR9.NKJ6LXNssIkhldurjP-uCrQbe__PNny9fHEaNmXRQDA'
```
Валюта по id
```bash
curl http://127.0.0.1:3000/api/v1/currencies/12 \
-H 'content-type: multipart/form-data' \
-H 'Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxLCJleHBpcnkiOjE1ODUwNzU5ODR9.NKJ6LXNssIkhldurjP-uCrQbe__PNny9fHEaNmXRQDA'

```

## Тесты (не завершено)
```bash
rails db:migrate RAILS_ENV=test
bundle exec rspec
```
