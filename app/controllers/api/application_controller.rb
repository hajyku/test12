module Api
  class ApplicationController < ActionController::API
    before_action :authenticate_request
    attr_reader :current_api_user

    private

    def authenticate_request
      @api_user = AuthorizeApiRequest.call(request.headers).result
      render json: { error: 'not a authorized' }, status: :unauthorized unless @api_user
    end
  end
end