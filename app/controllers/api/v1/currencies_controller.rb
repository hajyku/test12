class Api::V1::CurrenciesController < Api::ApplicationController
  def index
    currencies = Currency.all
    currencies = currencies.map do |c|
      { id: c.id, name: c.name, rate: c.rate }
    end

    render json: { results: currencies }.to_json, status: :ok
  end

  def show
    c = Currency.find(params[:id])
    render json: { name: c.name, rate: c.rate }
  end
end