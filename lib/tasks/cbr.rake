desc 'update currencies from cbr.ru'

task :cbr => :environment do

  raw = Hash.from_xml open('http://www.cbr.ru/scripts/XML_daily.asp')
  raw['ValCurs']['Valute'].each do |v|
    c = Currency.find_or_initialize_by(name: v['Name'])
    nominal = v['Nominal'].to_i || 1
    c.rate  = v['Value'].gsub(',', '.').to_f / nominal
    c.save!
  end

  puts 'Currencies updated at ' + Time.now.to_s

end