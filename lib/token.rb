class Token
  class << self

    def encode(payload, expiry = 24.hours.from_now)
      payload[:expiry] = expiry.to_i
      JWT.encode(payload, Rails.application.secrets.secret_key_base)
    end

    def decode(token)
      HashWithIndifferentAccess.new JWT.decode(token, Rails.application.secrets.secret_key_base)[0]
    rescue
      nil
    end

  end
end