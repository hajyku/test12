require 'rails_helper'

RSpec.describe Api::V1::CurrenciesController, type: :request do

  let!(:user) { User.create(email: 'test@test.com', password: 'test111') }

  context '#index', :vcr do
    it 'should 401 without auth' do
      get '/api/v1/currencies'
      expect(response.status).to eq(401)
    end

    it 'responds with JSON code 200 with JWT' do
      Currency.create(name: 'Тест1', rate:0.0)
      Currency.create(name: 'Тест2', rate:0.0)
      jwt = confirm_and_login_user(user)
      get '/api/v1/currencies', headers: { "Authorization" => "Bearer #{jwt}" }
      expect(response).to have_http_status(200)
      expect(json['results'].size).to eq(2)
    end
  end
end

def json
  JSON.parse(response.body)
end

def confirm_and_login_user(user)
  post '/api/v1/login', params: {email: user.email, password: user.password}
  return json['auth_token']
end